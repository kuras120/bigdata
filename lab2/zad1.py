from pyspark import SparkContext

# Init
context = SparkContext()

# Test
collection = ['a', 4, 'b', 3]
parallel_collection = context.parallelize(collection)

print(parallel_collection)
print(collection)

print('Kolekcja rozproszona dla 5: ', parallel_collection.take(5))
print('Kolekcja rozproszona dla count(): ', parallel_collection.take(parallel_collection.count()))

# Zad 1
# a)
print('Kolekcja rozproszona dla 3: ', parallel_collection.take(3))
print('Kolekcja rozproszona dla 1: ', parallel_collection.take(1))

# b)
print(parallel_collection.first())

# c)
p = context.parallelize([4, 3, 2, 8])
print(p.takeOrdered(4, lambda x: -x))
