from pyspark import SparkContext

# Init
context = SparkContext()

# 1)
a = context.parallelize([('a', 1), ('a', 2), ('b', 3), ('c', 4)])
b = context.parallelize([('a', 1), ('a', 10), ('b', 3), ('b', 40)])
c = a.cartesian(b)
print(c.take(c.count()))
# Uzycie flatMap do "wyplaszczenia" struktury
c_flat = c.flatMap(lambda x: (x[0], x[1]))
print(c_flat.take(c_flat.count()))
# Agregacja danych
aggregated_c_flat = c_flat.aggregateByKey(
    (0, 0),
    lambda init, value: (init[0] + 1, init[1] + value), lambda final, value: (final[0] + value[0], final[1] + value[1])
)
print(aggregated_c_flat.take(aggregated_c_flat.count()))
# Filtracja danych
filtered_aggregated_c_flat = aggregated_c_flat.filter(lambda x: x[0] == 'c')
print(filtered_aggregated_c_flat.take(filtered_aggregated_c_flat.count()))
