from pyspark.sql import SparkSession
from pyspark.sql.functions import avg


# Init
session = SparkSession.builder.master('spark://192.168.100.108:7077').getOrCreate()
context = session.sparkContext
df = session.read.options(header='True', delimiter=',').csv('co2_emission.csv')

df = df\
    .withColumnRenamed('Entity', 'Country')\
    .withColumnRenamed('Annual CO₂ emissions (tonnes )', 'Emission')

# Zmiana nazw kolumn
df.printSchema()

df = df \
    .withColumn('Year', df.Year.cast('date'))\
    .withColumn('Emission', df.Emission.cast('double'))

# Zmiana typow pol
df.printSchema()

# Print SQL
df.createOrReplaceTempView('emission')
session.sql('SELECT * FROM emission').show()

# Print API
df.select('*').show()

# Srednia emisja / kraj
df.select('Country', 'Emission').groupBy('Country').avg('Emission').show()

# Kraj o najwiekszej sredniej emisji CO2
df.select('Country', 'Emission').groupBy('Country').agg(avg('Emission').alias('AVG_Emission')).orderBy('AVG_Emission', ascending=False).show(1)

# Kraj o najwiekszej sredniej emisji CO2 wykluczajac swiat
df.select('Country', 'Emission').where(df.Country != 'World').groupBy('Country').agg(avg('Emission').alias('AVG_Emission')).orderBy('AVG_Emission', ascending=False).show(1)

input('Press enter to continue...')
session.stop()
