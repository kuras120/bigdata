from pyspark.sql import SparkSession

# Init
session = SparkSession.builder.master('spark://192.168.100.108:7077').getOrCreate()
context = session.sparkContext
df = context.parallelize([1, 2, 3, 4])
print(df.take(4))
input('Press enter to continue...')
session.stop()
