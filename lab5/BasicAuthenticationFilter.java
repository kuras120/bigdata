package com.neolitec.examples;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by kemanson on 12/02/14.
 */
public class BasicAuthenticationFilter implements Filter {

  /** Logger */
  private static final Logger LOG = LoggerFactory.getLogger(BasicAuthenticationFilter.class);

  private String realm = "Protected";
  private Map<String, String> users;

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
    List<String> paramNames = Collections.list(filterConfig.getInitParameterNames());
    users = new HashMap<>();
    for (var name: paramNames) {
      if (name.startsWith("user_")) {
        users.put(name.split("_", 2)[1], filterConfig.getInitParameter(name));
      }
    }

    String paramRealm = filterConfig.getInitParameter("realm");
    if (StringUtils.isNotBlank(paramRealm)) {
      realm = paramRealm;
    }
  }

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {

    HttpServletRequest request = (HttpServletRequest) servletRequest;
    HttpServletResponse response = (HttpServletResponse) servletResponse;

    String authHeader = request.getHeader("Authorization");
    if (authHeader != null) {
      StringTokenizer st = new StringTokenizer(authHeader);
      if (st.hasMoreTokens()) {
        String basic = st.nextToken();

        if (basic.equalsIgnoreCase("Basic")) {
          try {
            String credentials = new String(Base64.decodeBase64(st.nextToken()), "UTF-8");
            LOG.debug("Credentials: " + credentials);
            int p = credentials.indexOf(":");
            if (p != -1) {
              String _username = credentials.substring(0, p).trim();
              String _password = credentials.substring(p + 1).trim();
              boolean badCred = true;
              for (var user: users.entrySet()) {
                if (user.getKey().equals(_username) && user.getValue().equals(_password)) {
                  badCred = false;
                  break;
                }
              }
              if (badCred) {
                unauthorized(response, "Bad credentials");
              }

              filterChain.doFilter(servletRequest, servletResponse);
            } else {
              unauthorized(response, "Invalid authentication token");
            }
          } catch (UnsupportedEncodingException e) {
            throw new Error("Couldn't retrieve authentication", e);
          }
        }
      }
    } else {
      unauthorized(response);
    }
  }

  @Override
  public void destroy() {
  }

  private void unauthorized(HttpServletResponse response, String message) throws IOException {
    response.setHeader("WWW-Authenticate", "Basic realm=\"" + realm + "\"");
    response.sendError(401, message);
  }

  private void unauthorized(HttpServletResponse response) throws IOException {
    unauthorized(response, "Unauthorized");
  }

}
